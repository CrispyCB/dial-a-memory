jest.setTimeout(10000);
const server = require('../server/server');
const knex = require('../db/db');

const supertest = require('supertest');
const request = supertest(server);

const path = require('path');

beforeAll(() => knex.migrate.latest())

describe('file upload tests', () => {
    afterEach(() => {
        return knex('memories').where('file_name', "TheGhostOfYou.mp3").del();
    })
    it('should be able to upload a file', async () => {
        const filePath = path.join(__dirname, '/test_files/TheGhostOfYou.mp3');
        const response = await request.post('/memories/upload/TheGhostOfYou.mp3')
        .attach('file', filePath);
        expect(response.statusCode).toBe(201);
        expect(response.body.fileName).toBe("TheGhostOfYou.mp3");
        expect(response.body.fileURL).toBe("https://dial-a-memory-mp3-files.s3.amazonaws.com/TheGhostOfYou.mp3");
    })
})

describe('file delete tests', () => {
    beforeEach(() => {
       return knex('memories').insert({ 
            file_name: "TheGhostOfYou.mp3", 
            url: `https://dial-a-memory-mp3-files.s3.amazonaws.com/TheGhostOfYou.mp3`, 
            created_by: "CrispyBacon"
        })
    })
    it('should be able to delete a file', async () => {
        const response = await request.delete('/memories/delete/TheGhostOfYou.mp3');
        expect(response.statusCode).toBe(200);
    })
})