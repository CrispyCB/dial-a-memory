jest.setTimeout(10000);
const server = require('../server/server');
const knex = require('../db/db');

const supertest = require('supertest');
const request = supertest(server);

const path = require('path');

beforeAll(() => knex.migrate.latest())

describe('number handling tests', () => {
    // test passes, commenting out because Twilio test credentials
    // do not work for this route
    // it('should be able to get a list of phone numbers', async () => {
    //     const response = await request.get('/numbers/list')
    //     expect(response.statusCode).toBe(200)
    //     expect(response.body.phoneNumbers[0]).toHaveProperty('friendlyName')
    //     expect(response.body.phoneNumbers[0]).toHaveProperty('phoneNumber')
    // })
    it('should be able to provision a phone number', async () => {
        const response = await request.post('/numbers/create')
        .send({phoneNumber: "+15005550006", provisionedBy: "CrispyBacon"})
        expect(response.statusCode).toBe(201);
    })
    it('should be able to provision a phone number', async () => {
        const response = await request.post('/numbers/create')
        .send({phoneNumber: "+14704417345", provisionedBy: "CrispyBacon"})
        expect(response.statusCode).toBe(201);
    })
})