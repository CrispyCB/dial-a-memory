const server = require('../server/server');
const knex = require('../db/db');

const supertest = require('supertest');
const request = supertest(server);

beforeAll(() => knex.migrate.latest())

describe('file pairing tests', () => {
    afterEach(() => knex('pairings').where('paired_by', "CrispyBacon").del());
    it('should be able to pair a phone number and a file', async () => {
        const response = await request.post('/pairings/link')
        .send({
            phoneNumber: "(500) 555-0006",
            url: "https://dial-a-memory-mp3-files.s3.amazonaws.com/TheGhostOfYou.mp3",
            pairedBy: "CrispyBacon"
        });
        expect(response.statusCode).toBe(201);
    })
})