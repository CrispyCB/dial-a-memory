let knexConfig = require("./knexfile");
let configuredKnex = knexConfig[process.env.NODE_ENV || "development"];

const knex = require("knex")(configuredKnex);

module.exports = knex;
