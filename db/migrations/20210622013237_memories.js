
exports.up = function(knex, promise) {
    return knex.schema.createTable('memories', function(table){
        table.increments();
        table.string('file_name').notNullable().unique();
        table.string('url').notNullable().unique();
        table.string('created_by').notNullable();
        table.timestamps(false, true);
    })
};

exports.down = function(knex, promise) {
  return knex.schema.dropTable('memories');
};
