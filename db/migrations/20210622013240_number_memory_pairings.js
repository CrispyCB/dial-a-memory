
exports.up = function(knex, promise) {
  return knex.schema.createTable('pairings', function(table){
    table.increments();
    table.string('phone_number').unique().notNullable();
    table.string('url').unique().notNullable();
    table.string('paired_by').unique().notNullable();
    table.timestamps(false, true);
  })
};

exports.down = function(knex, promise) {
  return knex.schema.dropTable('pairings');
};
