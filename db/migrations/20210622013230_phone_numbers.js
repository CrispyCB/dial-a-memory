
exports.up = function(knex, promise) {
    return knex.schema.createTable('phone_numbers', function(table){
        table.increments();
        table.string('phone_number').notNullable().unique();
        table.string('provisioned_by').notNullable();
        table.timestamps(false, true);
    })
  
};

exports.down = function(knex, promise) {
  return knex.schema.dropTable('phone_numbers');
};
