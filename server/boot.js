
const dotenv = require('dotenv').config();
const server = require('./server');
const knex = require('../db/db');
const PORT = process.env.PORT || 8000;


server.listen(PORT, () => {
        console.log(`Dial-A-Memory backend running on port ${PORT}.`)
})