const express = require('express');
const parser = require('body-parser');

const server = express();
const file_handler = require('./routes/file');
const pairing_handler = require('./routes/pairing');
const number_handler = require('./routes/number')

// set up body-parser to use both
// url-encoded and generic JSON
// parsers so that all incoming
// requests are parsed.
server.use(parser.urlencoded({ extended: false }));
server.use(parser.json());

server.use('/memories', file_handler);
server.use('/pairings', pairing_handler)
server.use('/numbers', number_handler)

module.exports = server;