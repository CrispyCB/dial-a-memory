const knex = require("../../db/db")
const dotenv = require('dotenv').config()
const pinoHttp = require('pino-http')();


exports.link = async (req, res) => {
    try {
        const insert = await knex('pairings').insert({
            phone_number: req.body.phoneNumber,
            url: req.body.url,
            paired_by: req.body.pairedBy
        })
        if (insert) {
            pinoHttp.logger.info(`Successfully linked ${req.body.phoneNumber} and ${req.body.url}`);
            res.status(201).send(`Pairing created!`).end();
        }
    } catch (err) {
        pinoHttp.logger.error(err)
    }
}