const dotenv = require('dotenv').config();

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;

const pinoHttp = require('pino-http')();
const client = require('twilio')(accountSid, authToken);
const knex = require("../../db/db");


exports.get = async (req, res) => {
   let availableNumbersResponse = {
       phoneNumbers: []
   }
   await client.availablePhoneNumbers('US').local.list({limit: 5}).then(
       local => local.forEach(m => availableNumbersResponse.phoneNumbers.push({
           'friendlyName': m.friendlyName,
           'phoneNumber': m.phoneNumber
       }))
   )
   pinoHttp.logger.info(availableNumbersResponse);
   res.status(200).send(availableNumbersResponse);
}

exports.create = async (req, res) => {
    try{
        const createdNumber = await client.incomingPhoneNumbers.create({ phoneNumber: req.body.phoneNumber})
        if (createdNumber) {
            const insertedNumber = await knex('phone_numbers').insert({
                phone_number: req.body.phoneNumber,
                provisioned_by: req.body.provisionedBy
            });
            if (insertedNumber) {
                pinoHttp.logger.info(`Successfully provisioned ${createdNumber.phoneNumber}`);
                res.status(201).send(`Successfully activated ${createdNumber.friendlyName}!`).end();
            }
         }
    }catch (err) {
        pinoHttp.logger.error(err)
    }
}