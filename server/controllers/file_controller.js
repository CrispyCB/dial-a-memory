
const { PutObjectCommand, S3Client, DeleteObjectCommand } = require('@aws-sdk/client-s3');
const dotenv = require('dotenv').config();
const pinoHttp = require('pino-http')();
const fs = require('fs');
const path = require('path');
const knex = require('./../../db/db');

// construct new S3 object.
const s3Client = new S3Client({
    region: 'us-east-1',
    apiVersion: '2006-03-01'
});

exports.upload = async (req, res) => {
    const file = req.file;
    const fileStream = fs.createReadStream(file.path);
    fileStream.on('error', (err) => {
        pinoHttp.logger.error(err);
        res.status(400).send("Error in file stream creation").end();
    })
    let fileUploadParams = {
        Bucket: `${process.env.S3BUCKETNAME}`,
        Key: file.originalname,
        Body: fileStream
    }
    try {
        const fileUpload = await s3Client.send(new PutObjectCommand(fileUploadParams));
        const insert = await knex('memories').insert({
            file_name: file.originalname, 
            url: `https://${process.env.S3BUCKETNAME}.s3.amazonaws.com/${file.originalname}`, 
            created_by: "CrispyBacon"
        })
        if(fileUpload && insert) {
            let fileUploadResponse = {
                fileName: file.originalname,
                fileURL: `https://dial-a-memory-mp3-files.s3.amazonaws.com/${file.originalname}`
            }
            pinoHttp.logger.info(`Successfully uploaded ${fileUploadParams.Key} to ${fileUploadParams.Bucket}.`);
            res.status(201).send(fileUploadResponse).end();
        }
    } catch (err) {
        pinoHttp.logger.error(err)
    }
};

exports.delete = async (req, res) => {
    let fileDeleteParams = {
        Bucket: `${process.env.S3BUCKETNAME}`,
        Key: req.params.fileName
    }
    try {
        const fileDelete = await s3Client.send(new DeleteObjectCommand(fileDeleteParams));
        const del = await knex('memories').where('file_name', req.params.fileName).del();
        if (fileDelete && del) {
            let fileDeleteResponse = {
                fileName: req.params.fileName,
                message: `${req.params.fileName} successfully deleted.`
            }
            pinoHttp.logger.info(`Successfully deleted ${fileDeleteParams.Key}.`)
            res.status(200).send(fileDeleteResponse).end();
        }
    }  catch (err) {
        pinoHttp.logger.error(err)
    }
}