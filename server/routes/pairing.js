const express = require('express');
const router = express.Router();
const pairing = require('../controllers/pairing_controller');

router.post('/link', pairing.link);

module.exports = router;