const express = require('express');
const multer = require('multer');
const router = express.Router();
const file = require('../controllers/file_controller');
const upload = multer({ dest: '../tests/test-files'});

router.post('/upload/:fileName', upload.single('file'), file.upload);

router.delete('/delete/:fileName', file.delete);

module.exports = router;