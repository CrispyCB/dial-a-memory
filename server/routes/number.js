const express = require('express');
const router = express.Router();
const number = require('../controllers/number_controller');

router.get('/list', number.get);
router.post('/create', number.create);

module.exports = router;